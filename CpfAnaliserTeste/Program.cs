﻿using Cpf;
using System;

namespace CpfTeste
{
    /// <summary>
    /// Programa em si.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Método principal do programa.
        /// </summary>
        static void Main()
		{
			// Gera cpfs válidados offiline
			foreach (string cpf in Samples.GerarCpfsValidosOffiline(15))
			{ 
				Console.WriteLine($"Cpf gerado a partir do cálculo: {cpf}");
			}

			/*
			
			// Gera cpfs válidados online
			foreach (string cpf in Samples.GerarCpfsValidosOnline(1))
				Console.WriteLine($"Cpf gerado a partir da receita federal: {cpf}");

			// Descobre os digitos verificadores
			Console.WriteLine("Digitos verificadores: " + Samples.DescobrirDigitosVerficadores("008.758.371"));

			// Verifica se um dado cpf é correto offiline
			if (Samples.VerificarCpf("008.758.371-23")) Console.WriteLine("Verificando offiline: Cpf Válido");
			else Console.WriteLine("Verificando offiline: Cpf Inválido");

			// Verifica se um dado cpf é correto online
			if (Samples.VerificarCpfOnline("70238439123")) Console.WriteLine("Verificando online  : Cpf é Válido");
			else Console.WriteLine("Verificando online  : Cpf é Inválido");

			*/


			// Pausa o console
			Console.ReadLine();
        }
    }
}
