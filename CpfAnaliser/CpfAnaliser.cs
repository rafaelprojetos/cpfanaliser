﻿namespace Cpf
{
	/// <summary>
	/// Classe responsável pela analise de CPF.
	/// </summary>
	public class CpfAnaliser
	{
		/// <summary>
		/// Verifica se o CPF passado é válido a partir do digito verificador.
		/// </summary>
		/// <param name="cpf">CPF a ser verificado.</param>
		/// <returns>True == Valido, False == Inálido</returns>
		public static bool ValidateCpf(string cpf)
		{
			#region Remove todos os algarismos do cpf que não seja número

			cpf = CleanStringOfContainsCpf(cpf: cpf);

			#endregion

			#region Verifica entradas

			// Se o cpf não tiver o tamanho padrão para cpf, então é inválido.
			if (cpf.Length != 11)
			{
				return false;
			}

			#endregion

			#region Separa o cpf em numeros do cpf e numeros dos digitos verificadores

			string noveDigIniciaisDoCpf = cpf.Substring(startIndex: 0, length: 9);

			string digitosVerificadoresDoCpf = cpf.Substring(startIndex: 9, length: 2);

			#endregion

			#region Faz o calculo para saber se o cpf é realmente válido

			if (CpfCreate.GenerateNumberVerifier(noveDigIniciaisDoCpf) == digitosVerificadoresDoCpf)
			{
				return true;
			}

			return false;
			
			#endregion
		}

		/// <summary>
		/// Remove todos os algarismos do cpf que não seja número
		/// </summary>
		/// <param name="cpf"></param>
		/// <returns></returns>
		public static string CleanStringOfContainsCpf(string cpf)
		{
			string caracteresPossiveis = "0123456789";

			foreach (char letra in cpf)
			{
				// Se for um caracter diferente dos que é liberado, então remova-os por espaço em branco
				if (!caracteresPossiveis.Contains(letra.ToString()))
				{
					cpf = cpf.Replace($"{letra.ToString()}", "");
				}
			}

			return cpf;
		}

		/// <summary>
		/// Verifica se o CPF passado é válido a partir da API da receita federal.
		/// </summary>
		/// <param name="cpf">CPF a ser verificado.</param>
		/// <returns>True == Valido, False == Inálido</returns>
		public static bool ValidateCpfInApi(string cpf)
		{
			return true;
		}
	}
}
