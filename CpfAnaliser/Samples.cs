﻿using System.Collections.Generic;

namespace Cpf
{
	/// <summary>
	/// Classe usada apenas para exemplos de como consumir a biblioteca de classes em questão.
	/// </summary>
	public class Samples
	{
		/// <summary>
		/// Verifica se um dado CPF é válido.
		/// </summary>
		/// <param name="cpf">CPF a ser analisado.</param>
		public static bool VerificarCpf(string cpf)
		{
			return CpfAnaliser.ValidateCpf(cpf);
		}

		/// <summary>
		/// Verifica se um dado CPF é válido pelo cálculo e online na API da receita federal.
		/// </summary>
		/// <param name="cpf">CPF a ser analisado.</param>
		public static bool VerificarCpfOnline(string cpf)
		{
			return CpfAnaliser.ValidateCpfInApi(cpf);
		}

		/// <summary>
		/// Gera CPFs válidos a partir do calculo offiline.
		/// </summary>
		/// <param name="qtd">Quantidade de CPF's a serem gerados.</param>
		/// <returns>Uma lista com a quantidade desejada de CPFs.</returns>
		public static List<string> GerarCpfsValidosOffiline(int qtd)
		{
			return CpfCreate.GenerateCpfValidatesOffiline(qtd);
		}

		/// <summary>
		/// Gera CPFs válidos a partir da receita federal.
		/// </summary>
		/// <param name="qtd">Quantidade de CPF's a serem gerados.</param>
		/// <returns>Uma lista com a quantidade desejada de CPFs.</returns>
		public static List<string> GerarCpfsValidosOnline(int qtd)
		{
			return CpfCreate.GenerateCpfValidatesOnline(qtd);
		}

		/// <summary>
		/// Descobre os dois digitos verificadores de um dado cpf.
		/// </summary>
		/// <param name="noveDigitosIniciaisDoCpf">Os nove primeiros dígitos do cpf.</param>
		/// <returns></returns>
		public static string DescobrirDigitosVerficadores(string noveDigitosIniciaisDoCpf)
		{
			return CpfCreate.GenerateNumberVerifier(noveDigitosIniciaisDoCpf: noveDigitosIniciaisDoCpf);
			
		}
	}
}
