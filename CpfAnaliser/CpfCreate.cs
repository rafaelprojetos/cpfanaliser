﻿using System.Collections.Generic;
using System;
using System.Text;

namespace Cpf
{
    /// <summary>
    /// Classe responsável pela criação de CPF.
    /// </summary>
    public class CpfCreate
    {
		/// <summary>
		/// Gera CPFs válidos a partir do calculo offiline.
		/// </summary>
		/// <param name="qtd">Quantidade de CPFs válidos a serem gerados.</param>
		/// <returns>Uma lista com a quantidade desejada de CPFs.</returns>
		public static List<string> GenerateCpfValidatesOffiline(int qtd)
        {
			List<string> cpfs = new List<string>();
			Random objNumAleatorio = new Random();

			// Enquanto não gerar a quantidade pedida pelo usuário
			while(cpfs.Count < qtd)
			{
				string numAleatorio = objNumAleatorio.Next(0, 999999999).ToString();
				StringBuilder qtdDeZeros = new StringBuilder();
				if (numAleatorio.Length < 9)
				{
					int qtdDeZerosParaAdicionar = 9 - numAleatorio.Length;

					while(qtdDeZerosParaAdicionar != 0)
					{
						qtdDeZeros.Append("0");
						qtdDeZerosParaAdicionar--;
					}
				}

				string noveDigitosIniciaisDoCpf = qtdDeZeros + numAleatorio;

				string cpf = noveDigitosIniciaisDoCpf + GenerateNumberVerifier(noveDigitosIniciaisDoCpf: noveDigitosIniciaisDoCpf);

				if (!cpfs.Contains(cpf))
				{
					cpfs.Add(cpf);
				}
			}

			return cpfs;
		}

		/// <summary>
		/// Gera CPFs válidos a partir da receita federal.
		/// </summary>
		/// <param name="qtd">Quantidade de CPFs válidos a serem gerados.</param>
		/// <returns>Uma lista com a quantidade desejada de CPFs.</returns>
		public static List<string> GenerateCpfValidatesOnline(int qtd)
		{
			List<string> cpfs = new List<string>();

			return cpfs;
		}

		/// <summary>
		/// Gera o código verificador do noves primeiros digitos do cpf, ou seja
		/// o que deixa ele válido.
		/// </summary>
		/// <param name="noveDigitosIniciaisDoCpf">Noves primeiros digitos do cpf, 
		/// ou seja CPF sem os digitos verificadores.</param>
		/// <returns>Os dois digitos verificadores válidos para o cpf passado.</returns>
		public static string GenerateNumberVerifier(string noveDigitosIniciaisDoCpf)
		{
			#region Remove todos os algarismos do cpf que não seja número

			noveDigitosIniciaisDoCpf = CpfAnaliser.CleanStringOfContainsCpf(cpf: noveDigitosIniciaisDoCpf);

			#endregion

			#region Base do calculo

			int cpf_dig1 = 0;
			int cpf_dig2 = 0;
			int produto1 = 10;
			int produto2 = 11;

			foreach (char item in noveDigitosIniciaisDoCpf)
			{
				int item1 = int.Parse(item.ToString());

				cpf_dig1 += produto1 * item1;
				produto1--;

				cpf_dig2 += produto2 * item1;
				produto2--;
			}

			#endregion

			#region Primeiro digito verificador

			cpf_dig1 %= 11;
			cpf_dig1 = 11 - cpf_dig1;

			if (cpf_dig1 > 9)
			{
				cpf_dig1 = 0;
			}

			#endregion

			#region Segundo digito verificador

			cpf_dig2 += cpf_dig1 * produto2;
			cpf_dig2 %= 11;
			cpf_dig2 = 11 - cpf_dig2;

			if (cpf_dig2 > 9)
			{
				cpf_dig2 = 0;
			}

			#endregion

			return $"{cpf_dig1}{cpf_dig2}";
		}
	}
}
